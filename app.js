var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
var mongoose = require("mongoose")
var session = require('express-session')
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
var User = require('./modules/user.js');
var crypto = require('crypto');

mongoose.connect("mongodb://localhost/shop");

var routes = require('./routes/index');
var users = require('./routes/users');
var products = require('./routes/products');
var admin = require('./routes/admin');

var app = express();
const salt = "45%sAlT_";
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(busboyBodyParser({ limit: '5mb' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
  secret: 'SEGReT$25_',
  cookie : { maxAge : 60000},
  store: new MongoStore({ mongooseConnection: mongoose.connection }),
  resave: false,
  saveUninitialized: true
}));

passport.serializeUser(function(user, done) {
    done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  User
    .findOne({ _id : id})
    .exec((err, doc) => {
      if (!doc){
        return done("No user");
      } else {
        return done(null, doc);
      }
    })
});

app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(
  function(username,password, done){
      User
    		.findOne({username : username, password : hash(password)})
    		.exec((err, doc) => {
    			if (!doc){
            return done(null,false);
    			} else {
    		    return done(null, doc);
    			}
    		})
    }
));

app.use('/', routes);
app.use('/users', users);
app.use('/products', products);
app.use('/admin', admin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});



function hash(text) {
	return crypto.createHash('sha1')
	.update(text + salt).digest('base64')
}
// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});




module.exports = app;
