var mongoose = require('mongoose')
var crypto = require('crypto')
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
var Product = require('./modules/product.js');

exports.createProduct = function(req,res){
  var product = new Product();
  let image = req.files.img;
      console.log(image);
  image = image.data.toString('base64');
  product.title = req.body.title;
  product.collect = req.body.collect;
  product.price = req.body.price;
  product.description = req.body.description;
  product.img = image;
  product.save((err, data) =>{
    if(err){
      res.status(500).json(err)
    }
    res.redirect('/products/all_products');
  })
}


exports.deleteProduct = function(req, res){
  Product
  .remove()
    .exec((err, data) => {
      if(!err){
        let response = {
            message: `${req.body.title} successfully deleted`,
            id: req.body._id
        };
        //res.json(response);
        res.redirect('/products/all_products');
      }
      else {
        res.json({error : `Sorry you can"t delete ${req.body.title}`})
      }
    })
}

exports.modifyProduct = function(req, res){

  let image = req.files.img;
      console.log(image);
  image = image.data.toString('base64');
    Product
  .findAndModify({query : {title : req.body.title},
                    update : {$set : { title : req.body.title,
                                      img : image,
                                     collection : req.body.collection,
                                      price : req.body.price,
                                       description : req.body.description}},
                     new : false});
}
