var mongoose = require('mongoose');
const User = require('./user.js')
var product = new mongoose.Schema({
  title : {
      type: String,
      unique: true,
      required: true
  },
  img :{
    type : String
  },
  collect : {
    type : String
  },
  price : {
      type: Number,
      required: true
  },
  description : {
    type : String
  }
});

var ProductModel = mongoose.model('Product', product);

module.exports = ProductModel;
