var mongoose = require('mongoose');
const Product = require('./product.js')

var Schema = mongoose.Schema;
var User = new Schema({
  username : {
      type: String,
      unique: true,
      required: true
  },
  password : {
      type: String,
      required: true
  },
  name : {
    type: String,
    required: true
  },
  phone : {
    type: Number
  },
  type : {
    type: String,
    required: true
  },
  email : {
    type : String,
    unique : true,
    required: true
  },
  img : {
    type : String
  } ,
  purchase : [{type: mongoose.Schema.Types.ObjectId, ref:"Product"}]

});

var UserModel = mongoose.model('User', User);

module.exports = UserModel;
