var mongoose = require('mongoose');
const Product = require('./product.js').Schema
var comment = new mongoose.Schema({
  products : [{
      type: mongoose.Schema.Types.ObjectId,
      ref : 'Product'
  }],
  text :{
    type : String,
    required: true
  }

});

var CommentModel = mongoose.model('Comment', comment);

module.exports = {
  Model : CommentModel,
  Schema : comment
};
