function makeAjaxCall(){
  $.ajax('/users/login',{
    method: 'GET',
    success: function(data){
      console.log(data);
    }
  });
}

function getAllUsers() {
  $.ajax('/admin/users/',{
    method: 'GET',
    success: function(responseObject){
      var html = responseToHtml(responseObject);
      $('#all-users-container').html(html);
    }
  });
}

function responseToHtml(responseArray) {
  var resultHTML = '';
  for (var i = 0; i < responseArray.length; i++){
    var currentElement = responseArray[i];
    resultHTML +='<div class="thumbs">'+
    '<a href="/products/view/id/' +currentElement._id + '"><img width="200" height="200" src="data:image/png;base64,' + currentElement.img + '"> </a>' +
    '<div class="caption">'+
    '<span class="title">Назва :' + currentElement.title + '</span>'+
    '<span class="info">Ціна :' + currentElement.price + '</span>'+
    '</div>'+
    '</div>'
  }
  return resultHTML;
}
