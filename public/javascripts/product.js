function onSearchButtonClicked() {
  var searchText = $('#searchInput').val();
  searchProducts(searchText);
}

function searchProducts(searchText) {
  $.ajax('/products/product/?text=' + makeSecurityCheck(searchText),{
    method: 'GET',
    success: function(responseObject){
      $('#all-products-container').html(responseToHtml(responseObject));
    }
  });
}

function getAllProducts() {
  $.ajax('/products/all_products/all ',{
    method: 'GET',
    success: function(responseObject){
      var html = responseToHtml(responseObject);
      $('#all-products-container').html(html);
    }
  });
}

function responseToHtml(responseArray) {
  var resultHTML = '';
  for (var i = 0; i < responseArray.length; i++){
    var currentElement = responseArray[i];
    resultHTML +='<div class="thumbs">'+
    '<a href="/products/view/id/' +currentElement._id + '"><img width="200" height="200" src="data:image/png;base64,' + currentElement.img + '"> </a>' +
    '<div class="caption">'+
    '<span class="title">Назва :' + currentElement.title + '</span>'+
    '<span class="info">Ціна :' + currentElement.price + '</span>'+
    '</div>'+
    '</div>'
  }
  return resultHTML;
}

function deleteCartEntry(id) {
  $.ajax('users/products/buy/delete' + id,{
    method: 'POST',
    success: function(responseObject){
      location.reload();
    }
  });
}
