var mongoose = require('mongoose')
var crypto = require('crypto')
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
var User = require('./modules/user.js');
//var Product = require('./modules/product.js');


const salt = '45%sAlT_';
function hash(text) {
	return crypto.createHash('sha1')
	.update(text + salt).digest('base64')
}

exports.createUser = function(req, res){
	var us = new User();
	 let image = req.files.image.data.toString('base64');
  User
    .findOne({username: req.body.username})
    .exec((err, data) => {
      if (!data){
        us.password = hash(req.body.password);
        us.username = req.body.username;
        us.name = req.body.name;
        us.phone = req.body.phone;
        us.type = "user";
        us.email = req.body.email;
        us.img =  image;

        us.save((err, data)=>{
          if (err){
            res.status(500).json(err)
          }
          //res.json(data);
					res.redirect('/')
        });
      }
      else {
        res.json({error: `User with username ${req.body.username} already exists.`})
      }
    });
}

exports.deleteUser = function(req, res){
  User
  .remove(req.body._id)
    .exec((err, data) => {
      if(!err){
        let response = {
            message: `${req.body.username} successfully deleted`,
            id: req.body._id
        };
        res.json(response);
      }
      else {
        res.json({error : `Sorry you can"t delete ${req.body.username}`})
      }
    })
}

exports.modifyUser = function(req, res){
  User
    .findAndModify({query : {_id : req.body._id},
                    update : {$set : { name : req.body.name,
                                      username : req.body.username,
                                      password : req.body.password,
                                      img : img.data.toString('base64'),
                                     phone: req.body.phone,
                                      email : req.body.email}},
                     new : false});
}
