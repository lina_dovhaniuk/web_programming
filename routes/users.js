var express = require('express');
var router = express.Router();
const passport = require('passport');
var Product = require('../modules/product.js');
var User = require('../modules/user.js');
var api_user = require('../api_user.js')

/* GET users listing. */
router.get('/profile', function(req, res, next) {
  if(!req.user){
    res.redirect('/');
  }
  else if( req.user.type == "admin"){
    res.render('admin_page', {user : req.user});
  }
  else if(req.user.type == "user"){
    res.render('user_page', {user : req.user});
  }
});

router.get('/registration', function(req,res){
  res.render('registration');
})

router.post('/registration', function(req,res){
  api_user.createUser(req, res);
})

router.get('/login', function(req, res){
  res.render('login');
})

router.post('/login',
            passport.authenticate('local',{ failureRadirect : '/login'}),
            function(req, res, next) {
	res.redirect('./profile');
})

router.get('/logout', function(req,res,next){
  req.logout();
  res.redirect('/');
})

router.get('/cart', function(req,res){
  if(!req.user){
    res.redirect('/');
  }
  else if(req.user.type == "user"){
    User
      .findById(req.user._id)
      .populate('purchase')
      .exec((err, user) => {
        if (!err){
          res.render('cart', {user : req.user, purchases : user.purchase});
        }
        else res.status(500).end();
      })
  }

})

router.post('/modify', function(req, res){
  api_user.modifyUser(req,res);
})
router.get('/modify', function(req, res){
  res.render('modifyUser', {user : req.user});
})

router.post('/products/buy/delete/:id', function(req, res, next){
  if(!req.user)
    return res.redirect('/');
  User.findById(req.user._id)
    .exec((err, user) =>{
      if(!err){
        user.purchase = user.purchase.filter(purchase => purchase != req.params.id);
        user.save((err, updUser) => {
          res.redirect('/users/cart');
        })
      }
      else res.json(err);
    })
});


router.post('/products/buy/:id', function(req, res, next){
  User.update({'_id' : req.user._id}, {$push : {purchase : req.params.id}})
    .exec((err, doc) =>{
      if(err){
        res.json({err : "Didn`t update"})
      }
      console.log(doc);
    })

  User.distinct('purchase', function(err, doc){
    if(err){
      res.json({err : "Error"})
    }
  Product.find({'_id' : {$in : doc}}, function(err, purch){
    res.render('cart', {user : req.user, purchases : purch})
  })

  });
})


module.exports = router;
