var express = require('express');
var router = express.Router();
const passport = require('passport');
var api_user = require('../api_user.js')
var Product = require('../modules/product.js');
var User = require('../modules/user.js');
var api_product = require('../api_product.js')
var isValid = require('../security/security.js');


router.get('/all_products', function(req, res, next) {
  Product
    .find()
    .exec((err, doc) => {
      if (doc){
        res.render('all_products',{products: doc});
      } else {
      res.json({err :"Such a product isn`t available"});
      }
    })
});

router.get('/all_products/all', function(req, res, next) {
  Product
    .find()
    .exec((err, doc) => {
      if (doc){
        res.json(doc);
      } else {
      res.json({err :"Such a product isn`t available"});
      }
    })
});


router.get('/product/', function(req, res, next){
  console.log(req.query.text);
  if (isValid(req.query.text)){
let prod = Product;
if (req.query.text){
  prod = prod.find({title: new RegExp(req.query.text,'i')})
}
else prod = prod.find({});
    prod
      .exec((err, doc) => {
        if (doc){
          console.log('doc:' + doc + '\t' + req.query.text);
          res.json(doc);
        } else {
          res.json({err :"Such a product isn`t available"});
        }
      })
  }
})

router.get('/view/id/:id', function(req, res, next){
    Product.findById(req.params.id)
    .exec((err, doc) =>{
      if(doc){
        //res.json(doc);
        res.render('product_page', {product : doc, user : req.user});
      } else {
         res.json({err : "Such a product isn`t available"});
       }
    })
})

router.post('/delete/:id', function(req, res, next){
  console.log(req.params.id);
    Product.remove({_id : req.params.id})
    .exec((err, doc) =>{
      if(err){
        res.json({err : "Error"});
      }
    })
    Product.find()
    .exec((err, doc) =>{
      if(doc)res.render("all_products", {products : doc})
    })

})



module.exports = router;
