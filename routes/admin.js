var express = require('express');
var router = express.Router();
const passport = require('passport');
var api_user = require('../api_user.js');
var api_product = require('../api_product.js');
var Product = require('../modules/product.js');
var User = require('../modules/user.js');


router.post('/products/add', function(req, res, next) {
  api_product.createProduct(req, res);
});
router.get('/products/add', (req, res, next) => {
		if (!req.user) res.status(401).render('noAuthorized');
		else if (req.user.type !== 'admin') res.status(403).end('Forbidden');
		else next();
	},
    (req, res) => res.render('newProduct')
);


router.post('/products/view/:title', function(req, res, next) {
  api_product.modifyProduct(req, res);
});
router.get('/products/view/:title',(req, res, next) => {
		if (!req.user) res.status(401).render('noAuthorized');
		else if (req.user.type !== 'admin') res.status(403).end('Forbidden');
		else next();
	},
    (req, res) => res.render('modifyProduct')
);
router.get('/users', (req, res, next) => {
		if (!req.user) res.redirect('/');
		else if (req.user.type !== 'admin') res.status(403).end('Forbidden');
		else next();
	},
    (req, res) =>  {
      User
        .find()
        .exec((err, doc) => {
          if (doc){
            res.render('all_users', {users:doc});
          } else {
          res.json({err :"Such a product isn`t available"});
          }
        })
});


router.delete('/products/:_id', (req, res, next) => {
    		if (!req.user) res.status(401).render('noAuthorized');
    		else if (req.user.type !== 'admin') res.status(403).end('Forbidden');
    		else next();
    	},
        (req, res) =>  api_user.deleteProduct(req, res));

module.exports = router;
